#coding=utf-8
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()

#濃度:取得粉水比 14 15 16...越大越淡
#溫度:80-90...越高越濃
#顆粒:3-5...越大越淡
#速度:時間...越大越濃

#使用項目:粉水比 溫度 速度
X = [
	[160,78,60],
	[160,80,70],
	[150,81,80],
	[140,82,90],
	[130,85,100],
	[120,87,120],
	[120,90,120], 
	[120,88,120], 
]
scaler.fit(X)
X = scaler.transform(X)
y = [0,0,0,1,2,2,2,2]

clf = MLPClassifier(solver='lbfgs', alpha=1e-5,
                     hidden_layer_sizes=(5,3), random_state=1)

clf.fit(X, y)    

print clf.predict([[120,88,90]])
